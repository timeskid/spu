Installing SPU
==============

.. note::
  To make your life easier, please use `official Docker image <https://github.com/secretflow/spu#docker>`_.

SPU has been tested with the following settings:

- CentOS Linux 7 or later
- python3.8
- 16c64g

We have conducted some successful preliminary testings on macOS Monterey 12.4 with Intel processors, Apple Silicon is unsupported yet.


Download a Package
----------------------

You could install SPU via the official pip package:

.. code-block:: bash
  
  pip install spu



Build from Source
-----------------

Please check `README.md <https://github.com/secretflow/spu#build>`_ with `official Docker image <https://github.com/secretflow/spu#docker>`_.


